﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XFAutoCompleteBox.Controls;
using XFAutoCompleteBox.Droid;

[assembly: ExportRenderer(typeof(CommanEntry), typeof(CommanEntryRenderer))]
namespace XFAutoCompleteBox.Droid
{
    public class CommanEntryRenderer : EntryRenderer
    {
        public CommanEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var nativeEditText = Control as EditText;
            nativeEditText.SetBackgroundColor(Android.Graphics.Color.Transparent);

            if (nativeEditText != null)
            {
                nativeEditText.Gravity = GravityFlags.CenterVertical;
                nativeEditText.SetPadding(0, 0, 20, 0);

                if (e.NewElement is CommanEntry entry && entry.IsBorder)
                {
                    Control.Background = CustomDrawable(entry.BorderColor.ToAndroid(), entry.BorderRadius, entry.BorderWidth, entry.BackgroundColor.ToHex());
                }
            }

            //nativeEditText.Gravity = FormsConstants.AppFlowDirection == FlowDirection.RightToLeft
            //    ? GravityFlags.Right | GravityFlags.Center
            //    : GravityFlags.Left | GravityFlags.Center;

            try
            {
                if (Element is CommanEntry element)
                {
                    if (!string.IsNullOrEmpty(element.Placeholder))
                    {
                        nativeEditText.Hint = element.Placeholder;
                    }

                    IInputFilter[] FilterArr = new IInputFilter[1];
                    FilterArr[0] = new InputFilterLengthFilter(element.MaxLength);
                    Control.SetFilters(FilterArr);

                    if (element.IsCursorVisible)
                    {
                        SetCursorColor(element);
                    }
                    else
                    {
                        Control.SetCursorVisible(false);
                    }

                    if (!element.IsEnabled)
                    {
                        Control.SetTextColor(element.TextColor.ToAndroid());
                    }
                }


                //if (FormsConstants.AppFlowDirection == FlowDirection.RightToLeft)
                //{
                //    nativeEditText.TextAlignment = Android.Views.TextAlignment.TextEnd;
                //}
                //else
                //{
                //    nativeEditText.TextAlignment = Android.Views.TextAlignment.TextStart;
                //}
            }
            catch (Exception ex)
            {
                //ex.LogException();
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var entry = sender as CommanEntry;

            switch (e.PropertyName)
            {
                case nameof(Entry.TextColor):
                    if (entry.IsCursorVisible)
                    {
                        SetCursorColor(entry);
                    }
                    break;
                case nameof(CommanEntry.BorderColor):
                case nameof(CommanEntry.BorderWidth):
                case nameof(CommanEntry.BorderRadius):
                    if (entry.IsBorder)
                    {
                        Control.Background = CustomDrawable(entry.BorderColor.ToAndroid(), entry.BorderRadius, entry.BorderWidth, entry.BackgroundColor.ToHex());
                    }
                    break;
                case nameof(Entry.IsEnabled):
                    Control.SetTextColor(entry.TextColor.ToAndroid());
                    break;
            }
        }

        private void SetCursorColor(CommanEntry entry)
        {
            try
            {
                IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(TextView));
                IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0);
            }
            catch (Exception ex)
            {
                //ex.LogException();
            }
        }

        public Drawable CustomDrawable(Android.Graphics.Color strokeColor, int cornerRadius, int strokeWidth, string fillColor = "", bool isOval = false)
        {
            GradientDrawable drawable = new GradientDrawable();
            drawable.SetShape(isOval ? ShapeType.Oval : ShapeType.Rectangle);
            drawable.SetStroke(DpToPixels(strokeWidth), strokeColor);

            if (!isOval)
            {
                drawable.SetCornerRadius(DpToPixels(cornerRadius));
            }

            if (!string.IsNullOrEmpty(fillColor))
            {
                drawable.SetColor(Android.Graphics.Color.ParseColor(fillColor));
            }
            return drawable;
        }

        public static int DpToPixels(int dp)
        {
            return (int)(dp *  global::Android.App.Application.Context.Resources.DisplayMetrics.Density);
        }

        public static int DpToPixels(float dp)
        {
            return (int)(dp * global::Android.App.Application.Context.Resources.DisplayMetrics.Density);
        }
    }
}
