﻿using Xamarin.Forms;
using XFAutoCompleteBox.ViewModels;

namespace XFAutoCompleteBox.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainViewModel();
        }
    }
}
