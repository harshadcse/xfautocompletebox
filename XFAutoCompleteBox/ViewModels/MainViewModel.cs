﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace XFAutoCompleteBox.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public MainViewModel()
        {
            BindData();
        }

        public void BindData()
        {
            var strSuggestionArr = new string[] {
                   "harshad@mobmaxime.com",
                   "sagar.p@mobmaxime.com",
                   "kapil@mobmaxime.com",
                   "harry@test.com",
                   "xamarin@test.com",
                   "xamarinteam@mob.com"
            };

            SuggestionItem = new ObservableCollection<string>(strSuggestionArr.ToList());
        }

        ObservableCollection<string> _suggestionItem;
        public ObservableCollection<string> SuggestionItem
        {
            get => _suggestionItem;
            set
            {
                _suggestionItem = value;
                OnPropertyChanged();
            }
        }


        string _suggestionText;
        public string SuggestionText
        {
            get => _suggestionText;
            set
            {
                _suggestionText = value;
                OnPropertyChanged();
            }
        }
    }
}
