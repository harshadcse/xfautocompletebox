﻿using Xamarin.Forms;
namespace XFAutoCompleteBox.Controls
{
	public class CommanEntry : Entry
	{
		public static readonly BindableProperty IsBorderProperty =
			BindableProperty.Create(nameof(IsBorder), typeof(bool), typeof(CommanEntry), false);

		public static readonly BindableProperty BorderWidthProperty =
			BindableProperty.Create(nameof(BorderWidth), typeof(int), typeof(CommanEntry), 1);

		public static readonly BindableProperty BorderRadiusProperty =
			BindableProperty.Create(nameof(BorderRadius), typeof(int), typeof(CommanEntry), 0);

		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(CommanEntry), Color.Gray);

		public static readonly BindableProperty IsCursorVisibleProperty =
			BindableProperty.Create(nameof(IsCursorVisible), typeof(bool), typeof(CommanEntry), true);

		public bool IsBorder
		{
			get { return (bool)GetValue(IsBorderProperty); }
			set { SetValue(IsBorderProperty, value); }
		}

		public int BorderWidth
		{
			get { return (int)GetValue(BorderWidthProperty); }
			set { SetValue(BorderWidthProperty, value); }
		}

		public int BorderRadius
		{
			get { return (int)GetValue(BorderRadiusProperty); }
			set { SetValue(BorderRadiusProperty, value); }
		}

		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		public bool IsCursorVisible
		{
			get { return (bool)GetValue(IsCursorVisibleProperty); }
			set { SetValue(IsCursorVisibleProperty, value); }
		}
	}
}
