﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace XFAutoCompleteBox.Controls
{
    public partial class AutoCompleteBox : Grid
    {
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
                                                                        nameof(ItemsSource),
                                                                        typeof(IEnumerable<object>),
                                                                        typeof(AutoCompleteBox));

        public IEnumerable<object> ItemsSource
        {
            get { return (IEnumerable<object>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly BindableProperty TextProperty =
              BindableProperty.Create(nameof(Text), typeof(string), typeof(AutoCompleteBox), string.Empty);//, BindingMode.TwoWay, null, HandleBindingPropertyChangedDelegate);

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(AutoCompleteBox), string.Empty);

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(AutoCompleteBox), Color.Black);

        public static readonly BindableProperty PlaceholderColorProperty =
            BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(AutoCompleteBox), Color.Black);

        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create(nameof(FontSize), typeof(double), typeof(AutoCompleteBox), 17d);

        public static readonly BindableProperty ReturnTypeProperty =
            BindableProperty.Create(nameof(ReturnType), typeof(ReturnType), typeof(AutoCompleteBox), ReturnType.Default);

        public static readonly BindableProperty KeyboardProperty =
            BindableProperty.Create(nameof(Keyboard), typeof(Keyboard), typeof(AutoCompleteBox), Keyboard.Default, coerceValue: (o, v) => (Keyboard)v ?? Keyboard.Default);


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public Keyboard Keyboard
        {
            get { return (Keyboard)GetValue(KeyboardProperty); }
            set { SetValue(KeyboardProperty, value); }
        }

        public AutoCompleteBox()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName.Equals(ItemsSourceProperty.PropertyName))
            {
                ItemsSource = ItemsSource;
            }
        }

        void Entry_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 0)
            {
                listView.IsVisible = true;
                var filterItem = ItemsSource.Where(x => (x as string).ToLower().Contains(e.NewTextValue.ToLower())).ToList();
                listView.ItemsSource = filterItem;
                if(filterItem != null)
                {
                    listView.HeightRequest = filterItem.Count * 45 > 200 ? 200 : filterItem.Count * 45;
                }
            }
            else
            {
                listView.IsVisible = false;
            }
        }

        void listView_ItemSelected(System.Object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            if (sender is ListView lv) lv.SelectedItem = null;

            var tt = e.SelectedItem as string;
            entry.Text = tt;
            listView.IsVisible = false;
        }
    }
}
