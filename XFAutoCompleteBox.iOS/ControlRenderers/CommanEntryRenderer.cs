﻿using System;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.ComponentModel;
using XFAutoCompleteBox.Controls;
using XFAutoCompleteBox.iOS;

[assembly: ExportRenderer(typeof(CommanEntry), typeof(CommanEntryRenderer))]
namespace XFAutoCompleteBox.iOS
{
	public class CommanEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || e.NewElement == null || Control == null)
			{
				return;
			}

			if (e.NewElement is CommanEntry element && Control != null)
			{
				//switch (element.HorizontalTextAlignment)
				//{
				//	case TextAlignment.Start:
				//		Control.TextAlignment = FormsConstants.AppFlowDirection == FlowDirection.LeftToRight ? UITextAlignment.Left : UITextAlignment.Right;
				//		break;
				//	case TextAlignment.End:
				//		Control.TextAlignment = FormsConstants.AppFlowDirection == FlowDirection.LeftToRight ? UITextAlignment.Right : UITextAlignment.Left;
				//		break;
				//}

				SetBorder(element);
				SetCursorColor(element);

				if (!element.IsEnabled)
				{
					Control.TextColor = element.TextColor.ToUIColor();
				}

				Control.ShouldChangeCharacters += delegate (UITextField textField, NSRange range, string replacementString) {
					if (range.Length > 0)
					{
						return true;
					}
					if (range.Location >= element.MaxLength)
					{
						return false;
					}
					return true;
				};
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var entry = sender as CommanEntry;

			switch (e.PropertyName)
			{
				case nameof(Entry.TextColor):
					SetCursorColor(entry);
					break;
				case nameof(CommanEntry.BorderColor):
				case nameof(CommanEntry.BorderWidth):
				case nameof(CommanEntry.BorderRadius):
					SetBorder(entry);
					break;
				case nameof(Entry.IsEnabled):
					Control.TextColor = entry.TextColor.ToUIColor();
					break;
			}
		}

		private void SetBorder(CommanEntry entry)
		{
			if (entry.IsBorder)
			{
				Control.BorderStyle = UITextBorderStyle.Line;
				Control.Layer.CornerRadius = entry.BorderRadius;
				Control.Layer.BorderWidth = entry.BorderWidth;
				Control.Layer.BorderColor = entry.BorderColor.ToCGColor();
			}
			else
			{
				Control.BorderStyle = UITextBorderStyle.None;
			}
		}

		private void SetCursorColor(CommanEntry entry)
		{
			Control.TintColor = entry.IsCursorVisible ? entry.TextColor.ToUIColor() : UIColor.Clear;
		}
	}
}
